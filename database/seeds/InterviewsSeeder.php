<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class InterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date' =>  Carbon::now(),
                'text' => 'the interview went well'
            ],
            [
                'date' =>  Carbon::now(),
                'text' => 'the interview was bad'
            ],]);       
    }
}
