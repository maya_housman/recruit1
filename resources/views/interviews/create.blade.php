@extends('layouts.app')

@section('title', 'Create interview')

@section('content')
        <h1>Create interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name" class="col-md-4 col-form-label text-md-right">interview date</label>
            <input type = "date" class="form-control" name = "date">
        </div>     
        <div class="form-group">
            <label for = "email" class="col-md-4 col-form-label text-md-right">interview text</label>
            <input type = "text" class="form-control" name = "text">
        </div>      
        <div class="form-group">
            <label for="candidate_id" class="col-md-4 col-form-label text-md-right">Candidate</label>
                <select class="form-control" name="candidate_id">                                                                         
                    @foreach ($candidates as $candidate)
                        <option value="{{ $candidate->id }}"> 
                            {{ $candidate->name }} 
                        </option>
                    @endforeach    
                    </select>
        </div>
        <div class="form-group">
            <label for="candidate_id" class="col-md-4 col-form-label text-md-right">User</label>
                <select class="form-control" name="user_id">                                                                         
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}"> 
                            {{ $user->name }} 
                        </option>
                    @endforeach    
                    </select>
        </div>
                        
            <input type = "submit" name = "submit" value = "Create interview">


        </form>    
@endsection
