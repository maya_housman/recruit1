@extends('layouts.app')

@section('title', 'Interviews')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
<h1>List of interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>date</th><th>text</th><th>candidate</th><th>user</th>
    </tr>
    <!-- the table data -->
    @if(empty($interviews))
            You have no interviews
        @endif
    @foreach($interviews as $interview)
    <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->date}}</td>
            <td>{{$interview->text}}</td>
            <td> @if(isset($interview->candidate_id))
                          {{$interview->candidate->name}}  
                        @else
                          Assign candidate 
                        @endif
           </td>
           <td> @if(isset($interview->user_id))
                          {{$interview->user->name}}  
                        @else
                          Assign user 
                        @endif
           </td>
            </tr>
    @endforeach
        
</table>
@endsection